<?php

namespace app\models;

use yii\Helpers\ArrayHelper; //שאלה 1 - סעיף ב
use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
		
	// this 'getCategories' weas added to make a dropDown list in the forms 
	//שאלה 1 - סעיף ב
	public static function getCategories()
	{
		$allcategories = self::find()->all();
		$allcategoriesArray = ArrayHelper::
					map($allcategories, 'id', 'name');
		return $allcategoriesArray;						
	}
}
