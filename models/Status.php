<?php

namespace app\models;

use yii\Helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	// this 'getStatuses' weas added to make a dropDown list in the forms
	//שאלה 1 - סעיף ב
	public static function getStatuses()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'id', 'name');
		return $allStatusesArray;						
	}
}
