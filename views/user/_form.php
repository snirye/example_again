<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;		//שאלה 3- סעיף א,ב
use app\models\Category;	//שאלה 3- סעיף א,ב

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--?= $form->field($model, 'id')->textInput() ?> -->

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'authkey')->textInput(['maxlength' => true]) ?> -->
	
	
	
	
	<!-- שאלה 3- סעיף א,ב -->
	
	<?php if (!$model->isNewRecord) { ?>
		<?= $form->field($model, 'role')->dropDownList(User::getRoles()) ?>
	<?php } else { ?>
		<div style="display:none;"><?= $model->isNewRecord ? $form->field($model, 'role')->textInput(['value'=>simpleUser]) : ''?></div>
	<?php } ?>
	
	
	<!-- שאלה 3- סעיף א,ב -->
	
	
	<!--          ($model,'name of var')                  (name of model::name of function) -->
		<?php if (!$model->isNewRecord) { ?>
		<?= $form->field($model, 'categoryId')->dropDownList(Category::getCategories()) ?>
	<?php } else { ?>
		<div style="display:none;"><?= $model->isNewRecord ? $form->field($model, 'categoryId')->textInput(['value'=>simpleUser]) : ''?></div>
	<?php } ?>
	
	
	
	
	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	
	

    <?php ActiveForm::end(); ?>

</div>
