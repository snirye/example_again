<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category; //שאלה 1- סעף ב
use app\models\Status;	//שאלה 1- סעף ב

/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	
	<!--שאלה 1 - סעיף ב -->
	<?= $form->field($model, 'categoryId')->dropDownList(Category::getCategories())?>

	<!--שאלה 1 - סעיף ב -->
    <!--?= $form->field($model, 'statusId')->dropDownList(Status::getStatuses())?> -->
	
	
	<!-- שאלה 1- סעיף ג -->
	
	<?php if($model->isNewRecord){ ?>
			<div style="display:none;"> <?= $form->field($model, 'statusId')->textInput(['value'=>2]) ?> </div>
	<? }  else{ ?>
			<?= $form->field($model, 'statusId')-> dropDownList(Status::getStatuses()) ?>
	<? } ?>

	
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
