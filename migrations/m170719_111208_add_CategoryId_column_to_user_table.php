<?php

use yii\db\Migration;

/**
 * Handles adding CategoryId to table `user`.
 */
class m170719_111208_add_CategoryId_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'categoryId', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'categoryId');
    }
}
